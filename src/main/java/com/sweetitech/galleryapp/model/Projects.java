package com.sweetitech.galleryapp.model;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by TasnimAnkon on 7/14/17.
 */
public class Projects {
    private String projectName;
    private LocalDate completeDate;
    private String developerName;

    private static final List<Projects> projectList = Arrays.asList(
            new Projects("ai_app_24px2x", LocalDate.of(2017,07,14), "Ankon")
    );

    public Projects () {

    }

    public Projects(String projectName, LocalDate completeDate, String developerName) {
        this.projectName = projectName;
        this.completeDate = completeDate;
        this.developerName = developerName;
    }

    public Projects returnObject(String projectName) {
        for (Projects projects : projectList) {
            if (projects.getProjectName().equals(projectName)) {
                return projects;
            }
        }
        return null;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public LocalDate getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(LocalDate completeDate) {
        this.completeDate = completeDate;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }
}


