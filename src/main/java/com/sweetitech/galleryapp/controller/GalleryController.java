package com.sweetitech.galleryapp.controller;

import com.sweetitech.galleryapp.model.Projects;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;

/**
 * Created by TasnimAnkon on 7/7/17.
 */

@Controller
public class GalleryController {

    @RequestMapping(value = "/projects/{projectName}")
    public String listOfImages(@PathVariable String projectName, ModelMap modelMap) {
        Projects p = new Projects();
        Projects projects = p.returnObject(projectName);
        modelMap.put("projects", projects);
        return "projects";
    }

    @RequestMapping(value = "/")
    public String homePage(ModelMap modelMap) {
        Projects projects = new Projects("ai_app_24px2x", LocalDate.of(2017,07,14), "Ankon");
        modelMap.put("projects", projects);
        return "home";
    }

}
